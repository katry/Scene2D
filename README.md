# Scene2D

WebGL 2D scene with Canvas2D fallback.

## Initialization
```js
let scene = new Scene2D(document.querySelector("canvas"));
```

## Objects
```js
let rect = scene.createRect(props);
let sprite = scene.createSprite(image, props);
let texture = scene.createTexture(image, props);
let animation = scene.createAnimation(images, props, 3); // [images], props, ticks_per_frame
```

## Props format
```json
{
	"size": {"x": 100, "y": 100}, //default 0,0 or image (sprite/animation)
	"position": {"x": 100, "y": 100},  // default 0,0
	"center": {"x": 50, "y": 50},  // default 1/2*size
	"angle": 0, //angle in deg, default 0
	"priority": 1,  // default 1
}
```

## Object methods & properties
```js
[all].size = {x: 100, y: 100};
[all].position = {x: 100, y: 100};
[all].center = {x: 50, y: 50};
[all].angle = 10;  //angle in deg
[all].angleRad = Math.PI;  //angle in rad

[all].delete();  // delete object from scene memory

rect.color = "#FFF"; //color in css format

sprite.texture = image; //loaded image set as texture
```

## scene methods & properties
```js
scene.render();  //render scene
scene.flush();  //flush scene

scene.background = "#FFF"; //color in css format or loaded image
```

## loader (optional)
```js
var loader = new Loader();
var background = loader.image("/img/backgrounds/Cartoon_Forest_BG_02.png"); //returns image
loader.check(); // start checking load status in loop
```
You can also use addEventListener method with further events:
- "load" - all content loaded
- "error" - loading error
- "itemLoad" - item loaded

## dev comments
------------
alpha build: terser src/scene2d.js src/kelement.js src/kanimation.js src/legacy/* src/gl/* -o build/Scene2D.min.js
