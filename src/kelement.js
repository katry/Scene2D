"use strict";

class KElement {
	constructor(scene, props, program=null, extern=false) {
		if (new.target === KElement)
			throw new TypeError("Cannot construct abstract class KElement");

		this.scene = scene;
		this.ctx = scene.ctx;
		this.extern = extern;
		this.program = program;
		this.buffers = {};
		this._uniforms = props._uniforms || {};
		this._size = new Coordinates({x: 0, y: 0}, false);
		let center = new Coordinates(props.center || {x: 0, y: 0}, true);
		if(props.size && props.size.x && props.size.y) {
			this._size = new Coordinates(props.size);
			center = new Coordinates(props.center || {x: props.size.x/2, y: props.size.y/2}, true);
		}
		this._position = new Coordinates(props.position || {x: 0, y: 0});
		this._rotations = [new Rotation(props.angle || 0, center)];
		this._rotations[0].attach(this);
		this.priority = props.priority || 0;
		this._enabled = props.enabled || true;
	}

	render(vertices=null) {
		if(this._enabled && this.scene.isVisible(this)) {
			this._render(vertices);
		}
	}

	delete(move) {
		this.scene.removeElement(this);
	}

	enable() { this._enabled=true; }
	disable() { this._enabled=false; }

	resize() {}

	updateRotation() {}
	sceneOffset() {}

	set priority(v) {
		if(this._priority === v || this.extern) return;
		if(this._priority !== undefined)
			this.delete(true);
		if(v < 0) this._priority = 0;
		else if (v > 1E6) this._priority = 1E6;
		else this._priority = v;
		if(this.scene.priorities.indexOf(this._priority) === -1) {
			this.scene.priorities.push(this._priority);
			this.scene.priorities.sort((a, b) => a - b);
			this.scene.elements[this._priority] = [];
		}
		this.scene.elements[this._priority].push(this);
	}

	get priority() {
		return this._priority;
	}

	set size(v) {
		this._size.setPoint(v);
	}

	get size() {
		return this._size;
	}

	set position(v) {
		this._position.setPoint(v);
	}

	get position() {
		return this._position;
	}

	set center(v) {
		this._rotations[0].center.setPoint(v);
	}

	get center() {
		return this._rotations[0].center;
	}

	set angle(v) {
		if(!Number.isFinite(v)) throw new TypeError("Angle must be number type");
		this._rotations[0].angle = v;
	}

	get angle() {
		return this._rotations[0].angle;
	}

	set rad(v) {
		if(!Number.isFinite(v)) throw new TypeError("Angle must be number type");
		this._rotations[0].rad = v;
	}

	get rad() {
		return tthis._rotations[0].rad;
	}
}
