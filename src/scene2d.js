"use strict";

class Scene2D {
	constructor(canvas, debug=false) {
		if(!(canvas instanceof HTMLCanvasElement)) throw new TypeError("Invalid canvas element");
		if(debug)
			return debug === "2d" ? new Scene2D_Legacy(canvas.getContext('2d')) : new Scene2D_GL(canvas.getContext(debug));

		if(document.createElement('canvas').getContext('webgl2')) {
			console.log("webgl2 renderer");
			return new Scene2D_GL(canvas.getContext('webgl2'));
		}
		if(document.createElement('canvas').getContext('webgl')) {
			console.log("webgl renderer");
			return new Scene2D_GL(canvas.getContext('webgl'));
		}
		else return new Scene2D_Legacy(canvas.getContext('2d'));
	}
}

//TODO: autoscale resolution on fullscreen
class Scene2D_Abstract {
	constructor(ctx, size) {
		if (new.target === Scene2D_Abstract)
			throw new TypeError("Cannot construct abstract class Scene2D_Abstract");
		this._events = [];
		this.ctx = ctx;
		this._offset = new Coordinates({x: 0, y: 0});
		this._size = {x: ctx.canvas.width, y: ctx.canvas.height};
		this.elements = {};
		this.textures = {};
		this.priorities = [];
		this.dynamicBackground = false;
	}

	removeElement(element) {
		if(element.priority < 0) return;
		let index = this.elements[element.priority].indexOf(element);
		if(index !== -1) {
			this.elements[element.priority].splice(index, 1);
		}
	}

	createAnimation(sprites, props, multiply=1) {
		props.priority = props.priority || 1;
		let animation = new KAnimation(this, images, props, multiply);
		if(this.priorities.indexOf(props.priority) === -1) {
			this.priorities.push(props.priority);
			this.priorities.sort((a, b) => a - b);
			this.elements[props.priority] = [];
		}
		this.elements[props.priority].push(animation);
		return animation;
	}


	isVisible(element) {
		if(!(element instanceof KElement) && !(element instanceof KAnimation))
			throw new TypeError("Invalid element");
		return element.position.x < this.offset.x + this.size.x &&
						element.position.x + element.size.x > this.offset.x &&
						element.position.y < this.offset.y + this.size.y &&
						element.position.y + element.size.y > this.offset.y;
	}

	render() {
		this.clear();
		this.priorities.forEach((item) => {
			this.elements[item].forEach((e) => { e.render(); });
		});
	}

	rescale(dynamic=0, width=1600) {
		if(dynamic >= 1)
			this.size = {
				x: this.this.ctx.canvas.clientWidth/dynamic,
				y: this.ctx.canvas.clientHeight/dynamic
			};
		else this.size = {
			x: width,
			y: width/this.ctx.canvas.clientWidth*this.ctx.canvas.clientHeight
		};
	}

	flush() {
		for(let key in this.elements) {
			while(this.elements[key].length) {
				this.elements[key][0].delete();
			}
		}
		this.priorities = [];
		this.elements = {};
		this.textures = {};
	}

	set offset(v) {
		if(typeof v !== "object") throw new TypeError("Offset must be object");
		this._offset.x = v.x;
		this._offset.y = v.y;
	}

	get offset() {
		return this._offset;
	}

	set size(v) {
		if(typeof v !== "object" || !Number.isInteger(v.x) && !Number.isInteger(v.y))
			throw new TypeError("Size must be object of integers //{x: 0, y: 0}");
		this._size = v;
		this.ctx.canvas.width = v.x;
		this.ctx.canvas.height = v.y;
		for(let key in this.elements) {
			this.elements[key].forEach((e) => { e.resize(this._size); });
		}
	}

	get size() {
		return this._size;
	}
}

class Rotation {
	constructor(angle, center) {
		this.affects = [];
		this.angle = angle;
		this._center = new Coordinates(center, true, () => { this._affectAll(); });
	}

	attach(element) {
		this.affects.push(element);
		this._affect(element);
	}

	detach(element) {
		let i = this.affects.indexOf(element);
		if(i === -1) throw new ReferenceError("Rotation not attached to element");
		this.affects.splice(i, 1);
	}

	delete() {
		let e = null;
		while(e = this.affects.pop()) {
			let i = e.rotations.indexOf(this);
			e.rotations.splice(i, 1);
		}
	}

	setCenter(obj) {
		if(typeof obj != "object" || !Number.isFinite(obj.x) || !Number.isFinite(obj.y))
			throw new TypeError("Center must be an object");
		this._center.setPoint = {};
		this._affectAll();
	}

	_affectAll() {
		this.affects.forEach( e => this._affect(e) );
	}

	_affect(element) {
		element.updateRotation(this);
	}

	set angle(v) {
		if(!Number.isFinite(v)) throw new TypeError("Angle must be number type");
		this._angle = v*Math.PI/180;
		this._affectAll();
	}

	get angle() {
		return this._angle/Math.PI*180;
	}

	set rad(v) {
		if(!Number.isFinite(v)) throw new TypeError("Angle must be number type");
		this._angle = v;
		this._affectAll();
	}

	get rad() {
		return this._angle;
	}

	set center(v) {
		this._center.setPoint(v);
	}

	get center() {
		return this._center;
	}
}

class Coordinates {
	constructor(c, allowDecimal=false, setter=()=>{}) {
		if(typeof c !== "object") throw new TypeError("Coordinates must be an object");
		this.allowDecimal = allowDecimal;
		this.setter = setter;
		this.x = c.x;
		this.y = c.y;
	}

	_check(v) {
		if(!Number.isInteger(v) && !this.allowDecimal) throw new TypeError("Coordinate must be an integer");
		else if(!Number.isFinite(v)) throw new TypeError("Coordinate must be a number");
	}

	setPoint(obj) {
		if(typeof obj !== "object") throw new TypeError("Coordinates must be an object");
		this._check(obj.x);
		this._check(obj.y);
		this._x = obj.x;
		this._y = obj.y;
		this._setter(this._x, this._y);
	}

	set x(v) {
		this._check(v);
		this._x = v;
		this._setter(v, this._y);
	}

	get x() {
		return this._x;
	}

	set y(v) {
		this._check(v);
		this._y = v;
		this._setter(this._x, v);
	}

	get y() {
		return this._y;
	}

	set setter(v) {
		if(typeof v !== "function") throw new TypeError("Setter must be a function");
		this._setter = v;
		v(this._x, this._y);
	}

	get setter() {
		return this._setter;
	}
}
