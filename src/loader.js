"use strict";

class Loader {
	constructor(data=null) {
		this.status = {loaded: 0, total: 0};
		this.attempts = 5;
		this._listeners = {
			load: [], 
			itemLoad: [()=>this.status.loaded++],
			error: [()=>{if(++image.attempt===this.attempts) return; image.src=url;}]
		};

		if(data && typeof data === "object") {
			if(data.images instanceof Array) {
				data.images.array.forEach(e => { this.image(e); });
			}
		}
	}

	check() {
		if(this.status.loaded === this.status.total) {
			this._listeners["load"].forEach(e => e());
		}
		else setTimeout(() => this.check(), 100);
	}

	addEventListener(event, listener) {
		if(!this._listeners[event]) throw new TypeError("Invalid event.type");
		if(typeof listener !== "function") throw new TypeError("Listener bust be a function");
		this._listeners[event].push(listener);
	}

	removeEventListener(event, listener) {
		if(!this._listeners[event]) throw new TypeError("Invalid event.type");
		let index = this._listeners[event].indexOf(listener);
		if(index > -1) {
			this._listeners[event].splice(index,1);
		}
	}

	_loadItem(item) {
		this.status.total++;
		item.attempt = this.attempts;
		this._listeners["error"].forEach(e => item.addEventListener("error", e));
		this._listeners["itemLoad"].forEach(e => item.addEventListener("load", e));
	}

	image(url) {
		let image = new Image();
		this._loadItem(image);
		image.src = url;
		return image;
	}
	xhr(url) {
		let xhr = new new XMLHttpRequest();
		this._loadItem(xhr);
		xhr.open("GET", url);
		xhr.send();
		return xhr;
	}
	script(url) {
		let script = new Image();
		this._loadItem(script);
		script.src = url;
		return script;
	}
}
