"use strict";

class Scene2D_GL extends Scene2D_Abstract {
	constructor(ctx) {
		super(ctx);
		ctx.viewport(0, 0, this.size.x, this.size.y);
		ctx.enable(ctx.BLEND);
		ctx.blendFunc(ctx.ONE, ctx.ONE_MINUS_SRC_ALPHA);
		this._offset.setter = (x, y) => {
			for(let key in this.elements) {
				this.elements[key].forEach(e => e.sceneOffset(x, y));
			}
		};
		this._reset();
	}

	createRect(props, sceneOffset=true) {
		return new KRect_GL(this, props, sceneOffset);
	}

	createSprite(image, props, animated=false, sceneOffset=true) {
		return new KSprite_GL(this, image, props, animated, sceneOffset);
	}

	createTexture(image, props, sceneOffset=false, isBackground=false) {
		return new KSprite_GL(this, image, props, false, sceneOffset, true, isBackground);
	}

	clear() {
		if(this._backround instanceof KSprite_GL) {
			this._backround.render();
		}
		else {
			this.ctx.clear(this.ctx.COLOR_BUFFER_BIT);
		}
	}

	_reset() {
		this.indices = {rect: [] };
		this.verts = {rect: [] };
		this.centers = {rect: []};
		this.angles = {rect: []};
		this.colors = {rect: []};
	}

	set background(v) {
		if(v instanceof Image) {
			if(this.dynamicBackground) {
				let x = Math.ceil(this.size.x/v.width)+1;
				let y = Math.ceil(this.size.y/v.height)+1;
				this._background = new KSprite_GL(this, v, {size: {x: v.width*x, y: v.height*y}, priority: -1}, false, true, true, true);
				this._background.sizeScale = {x: x, y: y};
			}
			else
				this._backround = new KSprite_GL(this, v, {size: {x: this.size.x, y: this.size.y}, priority: -1}, false, false);
		}
		else {
			this._backround = this.parseColor(v);
			this.ctx.clearColor(this._backround[0], this._backround[1], this._backround[2], this._backround[3]);
		}
	}

	get background() {
		return this._backround;
	}

	parseColor(color) {
		if(color[0] === "#") {
			if(color.length === 4)
				return this._parseColor(color.substr(1).match(/.{1,1}/g), 15);
			else if(color.length === 5) {
				let c = color.substr(1).match(/.{1,1}/g);
				c[3] = parseInt(c[3], 16)/15;
				return this._parseColor(c);
			}
			else if(color.length === 7)
				return this._parseColor(color.substr(1).match(/.{1,2}/g));
			else if(color.length === 9) {
				let c = color.substr(1).match(/.{1,2}/g);
				c[3] = parseInt(c[3], 16)/255;
				return this._parseColor(c);
			}
		}
		else if(color.indexOf("rgba") === 0) {
			return this._parseColor(color.substr(5,color.length-6).replace(/\s/g, "").split(","), 255, 10);
		}
		else if (color.indexOf("rgb") === 0)
			return this._parseColor(color.substr(4,color.length-5).replace(/\s/g, "").split(","), 255, 10);
		else throw new TypeError("Invalid color!");
	}

	_parseColor(arr,num=255, radix=16) {
		arr = arr.slice();
		for(let i=0; i<3; i++) {
			arr[i] = parseInt(arr[i],radix)/num;
		}
		if(arr.length===4)
			arr[3] = Number(arr[3]);
		else arr.push(1);
		return arr;
	}

	_createProgram(vertex, fragment) {
		let vertexShader = this.ctx.createShader(this.ctx.VERTEX_SHADER);
		this.ctx.shaderSource(vertexShader, vertex);
		this.ctx.compileShader(vertexShader);

		let fragmentShader = this.ctx.createShader(this.ctx.FRAGMENT_SHADER);
		this.ctx.shaderSource(fragmentShader, fragment);
		this.ctx.compileShader(fragmentShader);

		let program = this.ctx.createProgram();
		this.ctx.attachShader(program, vertexShader);
		this.ctx.attachShader(program, fragmentShader);
		this.ctx.linkProgram(program);
		return program;
	}

	_createBuffer(data, program, variable, len=2, draw=false) {
		let location = this.ctx.getAttribLocation(program, variable);
		this.ctx.enableVertexAttribArray(location);
		let buffer = this.ctx.createBuffer();
		this._bufferData(buffer, data, this.ctx.ARRAY_BUFFER, location, len, draw);
		return [buffer, location];
	}

	_bufferData(buffer, data, buff_type, location, len=2, draw=false) {
		this.ctx.bindBuffer(buff_type, buffer);
		this.ctx.bufferData(buff_type, data, draw || this.ctx.DYNAMIC_DRAW);
		this.ctx.vertexAttribPointer(location, len, this.ctx.FLOAT, false, 0, 0);
	}

	_setUniform(prg, variable, data) {
		let psUniform = this.ctx.getUniformLocation(prg, variable);
		if(data.length > 0) {
			this.ctx["uniform"+data.length+"fv"](psUniform, data);
		}
	}

	flush() {
		if(this._backround instanceof KSprite_GL) {
			this.ctx.clearColor(0, 0, 0, 1);
			this.ctx.deleteProgram(this._backround.program);
			this._backround = null;
		}
		super.flush();
	}
}
