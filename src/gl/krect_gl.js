"use strict";

class KRect_GL extends KElement_GL {
	constructor(scene, props={}, sceneOffset=true) {
		let program = scene._createProgram("attribute vec2 a_position;uniform int len;uniform float u_angles[128];uniform vec2 u_centers[128],u_offset,u_resolution,u_screenOffset;vec2 h(vec2 f,float d[128],vec2 e[128],vec2 g){vec2 c=f;for(int a=0;a<128;a++){if(a==len)break;vec2 b=e[a];b=a==1?e[a]-g:b,c=vec2(cos(d[a])*(c.x-b.x)-sin(d[a])*(c.y-b.y)+b.x,sin(d[a])*(c.x-b.x)+cos(d[a])*(c.y-b.y)+b.y);}return c;}vec2 i(vec2 a,vec2 b,float c[128],vec2 d[128],vec2 e){vec2 f=h(b,c,d,a),g=a-e+f;return g;}void main(){vec2 a=i(u_offset,a_position,u_angles,u_centers,u_screenOffset),b=a/u_resolution*2.-1.;gl_Position=vec4(b*vec2(1,-1),0,1);}", "precision mediump float;uniform vec4 u_color;void main(){gl_FragColor=u_color;}");
		super(scene, props, program, false, sceneOffset);
		this.color = props.color || "#000";
	}

	render() {
		super.render(6);
	}

	set color(v) {
		this._color = this.scene.parseColor(v);
		this.useProgram();
		this.scene._setUniform(this.program, "u_color", this._color);
		this._rawColor = v;
	}

	get color() {
		return this._rawColor;
	}
}
