"use strict";

class KSprite_GL extends KElement_GL {
	constructor(scene, image, props={}, animated=false, sceneOffset=true, isTexture=false, isBackground=false) {
		let program = scene._createProgram("attribute vec2 a_position,a_texCoord;uniform int len;uniform float u_angles[128];uniform vec2 u_centers[128],u_offset,u_screenOffset,u_resolution;varying vec2 h;vec2 i(vec2 f,float d[128],vec2 e[128],vec2 g){vec2 c=f;for(int a=0;a<128;a++){if(a==len)break;vec2 b=e[a];b=a==1?e[a]-g:b,c=vec2(cos(d[a])*(c.x-b.x)-sin(d[a])*(c.y-b.y)+b.x,sin(d[a])*(c.x-b.x)+cos(d[a])*(c.y-b.y)+b.y);}return c;}vec2 j(vec2 a,vec2 b,float c[128],vec2 d[128],vec2 e){vec2 f=i(b,c,d,a),g=a-e+f;return g;}void main(){vec2 a=j(u_offset,a_position,u_angles,u_centers,u_screenOffset),b=a/u_resolution*2.-1.;gl_Position=vec4(b*vec2(1,-1),0,1),h=a_texCoord;}", "precision mediump float;uniform sampler2D image;varying vec2 h;void main(){gl_FragColor=texture2D(image,h),gl_FragColor.rgb*=gl_FragColor.a;}");
		super(scene, props, program, animated, sceneOffset);
		this.size = props.size || {x: image.width, y: image.height};
		if(!props.size) this.center = {x: this.size.x/2, y: this.size.y/2};
		this.type = isTexture ? this.ctx.REPEAT : this.ctx.CLAMP_TO_EDGE;
		this.isTexture = isTexture;
		this.isBackground = isBackground;
		this.texture = image;
		this.animated = animated;
		this.sizeScale = {x: 1, y: 1};
	}

	render() {
		let ctx = this.ctx;
		ctx.useProgram(this.program);
		ctx.bindBuffer(ctx.ARRAY_BUFFER, this.buffers.tex[0]);
		ctx.vertexAttribPointer(1, 2, ctx.FLOAT, false, 0, 0);
		ctx.bindTexture(ctx.TEXTURE_2D, this._texture);
		super.render(6, true);
	}

	sceneOffset(x, y) {
		if(!this.isTexture) {
			super.sceneOffset(x, y);
			return;
		}
		if(!this._sceneOffset) return;
		if(this.isBackground) {
			this.position.x = Math.floor(x/(this.size.x/this.sizeScale.x))*this.size.x/this.sizeScale.x;
			this.position.x = Math.floor(x/(this.size.y/this.sizeScale.y))*this.size.y/this.sizeScale.y;
		}
		this.useProgram();
		this.scene._setUniform(this.program, "u_screenOffset", [x, y]);
	}

	set texture(v) {
		if(!(v instanceof Image)) throw new Error("Sprite must be Image type");
		let postfix = this.isTexture ? "tex" : "";
		let path = v.src + postfix;
		if(this.scene.textures[path])
			this._texture = this.scene.textures[path];
		else {
			let ctx = this.ctx;
			this.useProgram();
			this._texture = ctx.createTexture();
			ctx.bindTexture(ctx.TEXTURE_2D, this._texture);
			ctx.texParameteri(ctx.TEXTURE_2D, ctx.TEXTURE_WRAP_S, this.type);
			ctx.texParameteri(ctx.TEXTURE_2D, ctx.TEXTURE_WRAP_T, this.type);
			ctx.texParameteri(ctx.TEXTURE_2D, ctx.TEXTURE_MIN_FILTER, ctx.NEAREST);
			ctx.texParameteri(ctx.TEXTURE_2D, ctx.TEXTURE_MAG_FILTER, ctx.NEAREST);
			ctx.texImage2D(ctx.TEXTURE_2D, 0, ctx.RGBA, ctx.RGBA, ctx.UNSIGNED_BYTE, v);
			this._image = v;
		}
		let x = 1.0, y = 1.0;
		if(this.isTexture) {
			x = this._size.x/v.width;
			y = this._size.y/v.height;
		}
		if(!this.buffers.tex)
			this.buffers.tex = this.scene._createBuffer(new Float32Array([0.0, 0.0, x, 0.0, 0.0, y, 0.0, y, x, 0.0, x, y]), this.program, "a_texCoord");
	}

	get texture() {
		return this._image;
	}
}
