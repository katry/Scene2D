"use strict";

class KElement_GL extends KElement {
	constructor(scene, props, program, animated=false, sceneOffset=true) {
		if (new.target === KElement_GL)
			throw new TypeError("Cannot construct abstract class KElement_GL");
		props._uniforms = {
			centers: scene.ctx.getUniformLocation(program, "u_centers"),
			angles: scene.ctx.getUniformLocation(program, "u_angles"),
			len: scene.ctx.getUniformLocation(program, "len"),
		};
		super(scene, props, program, animated);
		this.resize(scene.size);
		this.dynamicDraw = props.dynamicDraw || this.ctx.DYNAMIC_DRAW;
		this.buffers.size = scene._createBuffer(new Float32Array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]), program, "a_position", 2, this.dynamicDraw);
		this._sceneOffset = sceneOffset;


		this._size.setter = (x, y) => {
			this.useProgram();
			let sizeData = new Float32Array([0, 0, this._size.x, 0, 0, this._size.y, 0, this._size.y, this._size.x, 0, this._size.x, this._size.y]);
			this.scene._bufferData(this.buffers.size[0], sizeData, this.ctx.ARRAY_BUFFER, this.buffers.size[1], 2, this.dynamicDraw);
		};
		this._position.setter = (x, y) => {
			this.useProgram();
			this.scene._setUniform(this.program, "u_offset", [x, y]);
		};
		this.sceneOffset(scene.offset.x, scene.offset.y);
	}

	_render(vertices, prepared) {
		let ctx = this.ctx;
		if(!prepared)
			ctx.useProgram(this.program);
		ctx.bindBuffer(ctx.ARRAY_BUFFER, this.buffers.size[0]);
		ctx.vertexAttribPointer(0, 2, ctx.FLOAT, false, 0, 0);
		ctx.drawArrays(ctx.TRIANGLES, 0, vertices);
	}

	resize(size) {
		if(typeof size !== "object" || !Number.isInteger(size.x) || !Number.isInteger(size.y))
			throw new TypeError("Resolution must be object of integers //{x: 0, y: 0}");
		this.useProgram();
		this.scene._setUniform(this.program, "u_resolution", [size.x, size.y]);
	}

	updateRotation() {
		let centers = [];
		let angles = [];
		this._rotations.forEach(e => {
			centers.push(e.center.x, e.center.y);
			angles.push(e.rad);
		});
		this.useProgram();
		this.ctx.uniform1i(this._uniforms.len, angles.length);
		this.ctx.uniform1fv(this._uniforms.angles, angles);
		this.ctx.uniform2fv(this._uniforms.centers, centers);
	}

	useProgram() {
		this.ctx.useProgram(this.program);
	}

	delete(move) {
		if(!move)
			this.ctx.deleteProgram(this.program);
		super.delete(move);
	}

	sceneOffset(x, y) {
		if(!this._sceneOffset) return;
		this.useProgram();
		this.scene._setUniform(this.program, "u_screenOffset", [x, y]);
	}
}
