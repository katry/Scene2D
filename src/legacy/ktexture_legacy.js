"use strict";

class KTexture_Legacy extends KSprite_Legacy {
	constructor(scene, image, props, isBackground=false) {
		super(scene, image, props);
		this.texture = image;
		this.isBackground = isBackground;
		this.sizeScale = {x: 1, y: 1};
	}

	_setter(v) {
		let c = document.createElement("canvas").getContext("2d");
		c.canvas.width = this.size.x;
		c.canvas.height = this.size.y;
		for(let w=0; w*v.width<this.size.x; w++) {
			for(let h=0; h*v.height<this.size.y; h++) {
				c.drawImage(v, w*v.width, h*v.height);
			}
		}
		this.image = c.canvas;
	}

	render() {
		if(this.isBackground) {
			this.position.x = Math.floor(this.scene.offset.x/(this.size.x/this.sizeScale.x))*this.size.x/this.sizeScale.x;
			this.position.y = Math.floor(this.scene.offset.y/(this.size.y/this.sizeScale.y))*this.size.y/this.sizeScale.y;
		}
		super.render();
	}

	set texture(v) {
		this._setter(v);
		this._image = v;
	}

	get texture() {
		return this.image;
	}
}
