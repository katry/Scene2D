"use strict";

class KRect_Legacy extends KElement {
	constructor(scene, props) {
		super(scene, props);
		this.color = props.color || "#000";
		this._points = [];
	}

	render() {
		let ctx = this.ctx;
		this.rotate();
		let points = this._points;
		ctx.beginPath();
		ctx.moveTo(points[0][0],points[0][1]);
		ctx.lineTo(points[1][0],points[1][1]);
		ctx.lineTo(points[2][0],points[2][1]);
		ctx.lineTo(points[3][0],points[3][1]);
		ctx.lineTo(points[0][0],points[0][1]);
		ctx.fillStyle = this._style;
		ctx.fill();
	}

	rotate() {
		this._rotations.forEach((e,i) => {
			let x, y, width, height, xc=0, yc=0;
			if(i===0) {
				x = this._position.x-this.scene.offset.x;
				y = this._position.y-this.scene.offset.y;
				width = this._size.x;
				height = this._size.y;
				xc = x; yc = y;
			}
			else {
				x = this._points[0][0];
				y = this._points[0][1];
				width = this._size.x;
				height = this._size.y;
			}
			this._points = this.scene.rotateBy([
				[x, y],
				[x+width, y],
				[x+width, y+height],
				[x, y+height]
			], e._angle, {x: xc+e.center.x, y: yc+e.center.y});
		});
	}

	set color(v) {
		this._style = v;
	}

	get color() {
		return this._style;
	}
}
