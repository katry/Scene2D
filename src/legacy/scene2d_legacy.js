"use strict";

class Scene2D_Legacy extends Scene2D_Abstract {
	constructor(ctx) {
		super(ctx);
	}

	createRect(props) {
		return new KRect_Legacy(this, props);
	}

	createSprite(image, props, animated=false) {
		return new KSprite_Legacy(this, image, props, animated);
	}

	createTexture(image, props, sceneOffset=false, isBackground=false) {
		return new KTexture_Legacy(this, image, props, isBackground);
	}

	rotateBy(points, angle, center) {
		for(let i in points) {
			let x = Math.cos(angle)*(points[i][0]-center.x)
				-Math.sin(angle)*(points[i][1]-center.y)+center.x;
			let y = Math.sin(angle)*(points[i][0]-center.x)
				+Math.cos(angle)*(points[i][1]-center.y)+center.y;
			points[i] = [x,y];
		}
		return points;
	}

	clear() {
		if(this._backround instanceof Image) {
			this.ctx.drawImage(this._backround, 0, 0, this.size.x, this.size.y);
		}
		else {
			this.ctx.fillStyle = this._backround;
			this.ctx.fillRect(0, 0, this.size.x, this.size.y);
		}
	}

	flush() {
		this._backround = "#000";
		super.flush();
	}

	parseColor(color) { return color; }

	set background(v) {
		if(v instanceof Image && this.dynamicBackground) {
			let x = Math.ceil(this.size.x/v.width)+1;
			let y = Math.ceil(this.size.y/v.height)+1;
			this._background = this.createTexture(v, {size: {x: v.width*x, y: v.height*y}, priority: -1}, false, true);
			this._background.sizeScale = {x: x, y: y};
		}
		else this._backround = v;
	}

	get background() {
		return htis._bacground;
	}
}
