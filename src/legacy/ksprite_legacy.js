"use strict";

class KSprite_Legacy extends KElement {
	constructor(scene, image, props, animated=false) {
		super(scene, props, null, animated);
		if(!(image instanceof Image)) throw new Error("Sprite must be Image type");
		this.image = image;
		this.animated = animated;
	}

	render() {
		this.ctx.save();
		let center = {};
		this._rotations.forEach((e, i) => {
			if(i===0) {
				this.ctx.translate(
					this._position.x - this.scene.offset.x + e._center.x,
					this._position.y - this.scene.offset.y + e._center.y
				);
				center = e._center;
			}
			else {
				this.ctx.translate(
					this.scene.offset.x + e._center.x,
					this.scene.offset.y + e._center.y
				);
				center.x = e._center.x-this._position.x;
				center.y = e._center.y-this._position.y;
			}
			this.ctx.rotate(e._angle);
		});
		this.ctx.drawImage(this.image, -center.x, -center.y, this._size.x, this._size.y);
		this.ctx.restore();
	}
}
