"use strict";

class KAnimation {
	constructor(scene, sprites, props, multiply=1, sceneOffset=true) {
		if(!sprites.length) throw new ReferenceError("No frames given");
		this.scene = scene;
		this.current = -1;
		this.multiply = multiply;
		this.index = 0;
		this._frames = [];
		this._sceneOffset = sceneOffset;
		this._size = new Coordinates({x: 0, y: 0});
		let center = new Coordinates(props.center || {x: 0, y: 0}, true);
		if(props.size && props.size.x && props.size.y) {
			this._size = new Coordinates(props.size);
			center = new Coordinates(props.center || {x: props.size.x/2, y: props.size.y/2}, true);
		}
		this._rotations = [new Rotation(props.angle || 0, center)];
		this._rotations[0].attach(this);
		sprites.forEach(e => {
			if(!(e instanceof Image)) throw new TypeError("Invalid animation frames");
			let frame = scene.createSprite(e, props, true, sceneOffset);
			frame._rotations = [this._rotations[0]];
			this._frames.push(frame);
		});
		this._position = new Coordinates(props.position || {x: 0, y: 0});
		this._priority = props.priority || 0;
		this._enabled = props.enabled || true;

		this._size.setter = (x, y) => { this._updateFrames("size", {x: x, y: y}); };
		this._position.setter = (x, y) => { this._updateFrames("position", {x: x, y: y}); };
		this.sceneOffset(scene.offset.x, scene.offset.y);
	}

	render() {
		if(!this._enabled) return;
		if(++this.index === this.multiply) {
			this.current = this.current+1 < this._frames.length ? this.current + 1 : 0;
			this.index=0;
		}
		else if(this.current === -1) this.current = 0;
		this._frames[this.current].render();
	}

	reset() {
		this.current = -1;
	}

	enable() { this._enabled=true; }
	disable() { this._enabled=false; }

	resize(size) {
		this._frames.forEach(e => e.resize(size));
	}

	updateRotation() {
		this._frames.forEach(e => e.updateRotation());
	}

	delete() {
		this.scene.removeElement(this);
	}

	_updateFrames(property, value) {
		this._frames.forEach((sprite) => {
			sprite[property] = value;
		});
	}

	sceneOffset(x, y) {
		if(!this._sceneOffset) return;
		this._frames.forEach(e => e.sceneOffset(x, y));
	}

	get frames() {
		return this._frames;
	}

	set size(v) {
		this._size.setPoint(v);
	}

	get size() {
		return this._size;
	}

	set position(v) {
		this._position.setPoint(v);
	}

	get position() {
		return this._position;
	}

	set center(v) {
		this._rotations[0].center.setPoint(v);
	}

	get center() {
		return this._rotations[0].center;
	}

	set angle(v) {
		this._rotations[0].angle = v;
	}

	get angle() {
		return this._rotations[0].angle;
	}

	set rad(v) {
		this._rotations[0].rad = v;
	}

	get rad() {
		return this._rotations[0].rad;
	}

	set priority(v) {
		if(this._priority === v) return;
		if(this._priority !== undefined)
			this.delete(true);
		if(v < 0) this._priority = 0;
		else if (v > 1E6) this._priority = 1E6;
		else this._priority = v;
		if(this.scene.priorities.indexOf(this._priority) === -1) {
			this.scene.priorities.push(this._priority);
			this.scene.priorities.sort((a, b) => a - b);
			this.scene.elements[this._priority] = [];
		}
		this.scene.elements[this._priority].push(this);
	}

	get priority() {
		return this._priority;
	}
}
